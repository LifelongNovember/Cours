

```
<expr> ::= <num>
	| <expr> "+" <expr>
	| <expr> "-" <expr>
	| <expr> "*" <expr>
	| <expr> "/" <expr>
	| "(" <expr> ")"
<num> ::= "-" ? [ "0"-"9" ] +
```

Symboles terminaux : `"+"`, `"-"`, `"*"`, `"/"`, `"("`, `")"`, `"0-9"`
Symboles non-terminaux : `"<expr>"`, `"<num>"`.

```
E -> N
E -> E + E
E -> E - E
E -> E * E
E -> E / E
E -> (E)
N -> 1N'
N -> 2N'
[...]
N -> 9N'
N' -> 0N'
N' -> 1N'
N' -> ε
```
L'unité des nombres peut être vide à gauche `(N' -> ε)`, la grammaire est donc contextuelle.
Les expressions contenant plusieurs opérations, l'une étant `"+"` ou " et `"-"` et l'autre étant `"*"` ou `"/"` produisent un résultat différent en fonction des priorités, la grammaire est donc ambiguë.
On peut la désambiguïer en établissant un ordre de priorité implicite. 
<!--stackedit_data:
eyJoaXN0b3J5IjpbNjM4MDk2NjI5LC0xMTgyMTU1Mjk2LC01MD
I1NDU5OTIsLTE1NTQ2NjAwNDBdfQ==
-->