# Programmation web avancée
> nicopowa@gmail.com

> On abandonne les boucles for, on les remplace par des itérateurs.
> ```  javascript
> a = [1,2,3];
> for (let i = 0; i < 3; i++)
>  a[i] = a[i]* 2;
> -----
>  values = values.map(value => val * 2);
>  ```

### Charger une liste d'amis : https://monsite/friends

> ``` javascript
> let friends[].map(friend => {username: username})
> -----
> // Charger les amis dont le profil est public.
> friends.map[].filter(friend => friend.publicProfile) // publicProfile booléen
> ```

### Gestion des variables en JS vs ES6 :
> ``` javascript
> function main() {
>  let a = 5;
>  for(var i = 0; i < a; i++) {
>  }
>  i++; // OK
> }
>-----
> function main() {
 > let a = 5;
  >for(let i = 0; i < a; i++) {
  >}
>  i++; // Erreur
>}
>```

### Promise :

> ``` javascript
>function req(url) {
>  new Promise((resolve, reject) {
>    if(statusCode = 200) resolve();
>    else reject();
>  });
>}
>-----
>let r = await req(url);
>console.log("fini");
>-----
>req(url).then => (val => {
>    let("fini");
>  }) catch (err => {
>    let("erreur");
>});
>```

### Fonctions nommées

> ``` javascript
>function myFunc(a, func) {};
>-----
>var myFunc(a, func() {});
>-----
>var myFunc = (a, () => {});
>-----
>// Rigoureusement identiques
>```

Projet de transition d'images : Checker la méthode RAF

let a = document.createElement(...)

a.addEventListener('click", method)
event.preventDefault() (empêche l'évenement de s'executer de fois simultanément)

\<canvas>

Minifier : Closure Compiler

Chargement dynamique d'items de tableaux : clusterize

let myElement = document.createElement("")
document.body.appendChild(myElement)
myElement.style.width
	height
	backgroundColor

myElement.addEventListener(
	const onClick = event => {
		console.log("clicked")
		console.log(event.clientX)
	}
)
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTg5MTg1NTMzNSwxMDgyNjI4NDAsMTgyOD
c3NDUyOSwtMjIxNjA0MTA4LC05NTQ2MDksODE1MjQ1OTAzLC0x
OTA5MTYyNDcxLC03Mzk0NzgyNV19
-->