
$$ \text{\LARGE \sf  Compréhension de programmes} $$ $$ \text{\large \sf  Catégories de fichiers (gnuplot)} $$

$$\text{Antoine Denis}$$

## Documentation

 * `./README`
 * Fichiers contenus dans `./man/`, `./share/man/`, `./docs/`
 * `./PORTING`
 * `./FAQ.pdf`

## Identification / Version

 * `./VERSION`	
 * `./NEWS`
 * `./PATCHLEVEL`
 * `./PGPKEYS`
 * `./RELEASE_NOTES`
 * `./TODO`
 * `./BUGS`
 * `./CodeStyle`

## Compilation / Installation

 * `./INSTALL`
 * `./missing`
 * `./mkinstalldirs`
 * `Makefile(...) / GNUmakefile / config(ure)(...) / install(...)` (un peu partout)
 * `./compile`

## Utilisation

 * `./bin/gnuplot`
 * Fichiers dans `./demo` & `./tutorial`
 * `./share/gnuplotrc`

## Sources

###  Librairies : `./src/*.h`

### Code : `./src/*.c`
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE4OTU2NzY4MTYsMTY4NDAzNjk1MSwtMT
E4MDk0NzQ2OSw3MTMxMDQ3ODcsLTUyNDk4NDkxM119
-->